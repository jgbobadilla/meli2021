const ErrorResponse = (code='SERVER_ERROR', description = 'Internal error') => {
  return {
    code: code,
    message: description
  }
};

const addAuthorToResponse = content => ({
  ...content,
  author: {
    name: 'Gabriel',
    lastname: 'Bobadilla'
  }
});

module.exports = {
  ErrorResponse,
  addAuthorToResponse
};
