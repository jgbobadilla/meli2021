const express = require('express');
const helmet = require('helmet');
const compression = require('compression');

// Inner modules
const ItemsModule = require('./modules/items/itemModule');
const itemsRouter = ItemsModule().initModule();

const app = express();

app.use(helmet());
app.use(compression());

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
	res.setHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept');
	next();
});


//pre-flight requests
app.options('*', function(req, res) {
	res.send(200);
});

app.use('/api/items', itemsRouter);

module.exports = app;
