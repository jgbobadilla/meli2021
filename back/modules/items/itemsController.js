const express = require('express');
const ItemsProvider = require('./itemsProvider');
const {addAuthorToResponse, ErrorResponse} = require('./../../helpers/responseHelper');

const ItemsController = () => {
  
  const itemProvider = ItemsProvider();

  let router = express.Router();

  // Search
  router.get('/', async (req, res) => {
    const query = req.query.q || void 0
    console.info(`Requested query: ${query || 'None'}`)
  
    if (query) {
      try {
        const response = await itemProvider.searchItems(query);
  
        res.status(200)
          .send(addAuthorToResponse(response));
      } catch (error) {
        res.status(500)
          .json(ErrorResponse('SERVER_ERROR', 'An internal error ocurred fetching data.'));
      }
    } else {
      res.status(400)
        .json(ErrorResponse('NO_SEARCH_CRITERIA', 'You did not provide a search criteria. Make sure your request matches the /api/items/?q=:query pattern'));
    }
      
    res.end();
  });


  // Get item
  router.get('/:id', async (req, res) => {
    const itemId = req.params.id || void 0
    console.info(`Requested item: ${itemId || 'None'}`)

    if (itemId) {
      try {
        const response = await itemProvider.getItem(itemId);

        res.status(200)
          .send(addAuthorToResponse(response));
      } catch (error) {
        console.log(error);
        const { status } = error.response || {};

        if (status == 404) {
          res.status(404)
            .json(ErrorResponse('NOT_FOUND', 'The requested item does not exist.'));
        } else {
          res.status(500)
            .json(ErrorResponse('SERVER_ERROR', 'An internal error ocurred fetching data.'));
        }
      }
    } else {
      res.status(400)
        .json(ErrorResponse('NO_ITEM_ID', 'You did not provide an item id. Make sure your request matches the /api/items/:id pattern'));
    }

    res.end();
  });


  return {
    init: () => router
  };
}

module.exports = ItemsController;