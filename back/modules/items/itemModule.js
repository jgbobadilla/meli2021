const ItemsController = require('./itemsController');

const ItemsModule = () => {
  const itemsController = ItemsController();

  const initModule = () => {
    return itemsController.init();
  }

  return {
    initModule
  }
}

module.exports = ItemsModule;
