const axios = require('axios');

const ItemsProvider = () => {

  const searchItems = async (query, limit) => {
    try {
      const url = `https://api.mercadolibre.com/sites/MLA/search?q=${query}`;
      const {data} = await axios.get(encodeURI(url));
      const {results : items, filters} = data || {};

      console.info(`Requesting for: ${url}`);

      return {
        categories: extractCategories(filters),
        items: extractItems(items, limit)
      };
    } catch (err) {
      console.log(err);

      throw err;
    }
  }

  const getItem = async (itemId) => {
    try {
      const itemUrl = `https://api.mercadolibre.com/items/${itemId}`;
      const itemDescriptionUrl = `https://api.mercadolibre.com/items/${itemId}/description`;
      console.info(`Requesting for: ${itemUrl}`);
      console.info(`Requesting for: ${itemDescriptionUrl}`);

      const responses = await Promise.all([
        axios.get(encodeURI(itemUrl)),
        axios.get(encodeURI(itemDescriptionUrl))
      ]);

      [itemResponse, itemDescriptionResponse] = responses;

      const item = itemResponse.data;
      const description = itemDescriptionResponse.data;

      return {
        item: addDescriptionToItem(castToItem(item, true), description)
      };
    } catch (err) {
      console.log(err);

      throw err;
    }
  }

  const extractCategories = (filters = []) => {
    let categories = [];

    // Categories
    const categoryFilter = filters.find(f => f.id === "category");
    if (categoryFilter && categoryFilter.values && Array.isArray(categoryFilter.values)) {
      [ firstCategoryPath, ...otherCategoryPaths ] = categoryFilter.values;

      categories = (firstCategoryPath.path_from_root || [])
        .map(category => {
          return {
            id: category.id,
            name: category.name
          }
        })
        .filter(c => !!c);
    }

    return categories;
  }

  const extractItems = (items = [], limit) => {
    limit = (!limit || !Number.isInteger(limit) || limit < 1) ? 4 : limit;

    return items
      .slice(0, limit)
      .map(item => castToItem(item));
  }

  const castToItem = (mlItem = {}, includeQuantity = false) => {
    let item = {
      id: mlItem.id,
      title: mlItem.title,
      price: {
        currency: mlItem.currency_id,
        amount: mlItem.price,
        decimals: 2
      },
      picture: (mlItem.pictures && mlItem.pictures.length ? mlItem.pictures[0].url : void 0) || mlItem.thumbnail || '',
      condition: mlItem.condition,
      free_shipping: mlItem.shipping.free_shipping || false
    };

    if (includeQuantity) {
      item = {
        ...item,
        sold_quantity: mlItem.sold_quantity || 0
      };
    }

    return item;
  }

  const addDescriptionToItem = (item, description) => {
    return {
      ...item,
      description: description.plain_text || ''
    };
  }

  return {
    searchItems,
    getItem
  }
}

module.exports = ItemsProvider;
