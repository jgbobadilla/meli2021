# Mercado Libre Test

El proyecto se basa en la plantilla [Create React App](https://github.com/facebook/create-react-app).

Repositorio ubicado [en BitBucket](https://bitbucket.org/jgbobadilla/meli2021/).


# Estructura del proyecto

La implementación se divide en dos aplicaciones, una para el front end y otra para el backend, ubicadas en las carpetas respectivas */front* y */back* respectivamente.


## Iniciar backend (modo desarrollo)

Ubicarse en la carpeta *back* del proyecto, y ejecute los siguientes comandos para iniciar en modo desarrollo:

```
> yarn
> yarn start

```

El servidor iniciará en modo desarrollo, respondiendo peticiones desde http://localhost:3100.


## Iniciar front (modo desarrollo)

Ubicarse en la carpeta *front* del proyecto, y ejecute los siguientes comandos para iniciar en modo desarrollo:

```
> yarn
> yarn start

```

La app iniciará en modo local, puede visitarla en su navegador visitando [http://localhost:3000](http://localhost:3000).



# Decisiones de desarrollo


## Lenguaje

Tanto el front como el back se realizaron con Javascript, enfocado a ES6. Opté por Javascript en lugar de Typescript como parte del reto que me propuse para el desarrollo de la prueba.

El uso de definiciones funcionales en lugar del uso de clases se debe a gusto personal.


## Stack y organización

Para el backend se creó una sencilla aplicación con NodeJs y Express, pensada para añadir funcionalidad organizada en módulos independientes, que faciliten su escalabilidad, pero con la posibilidad de compartir funcionalidad común por medio de las funciones expuestas en la carpeta helpers.

El frontend es una aplicación construida con React 17, Sass y también organizada de tal manera que se separan los componentes en dos grupos:

* components: Contiene componentes independientes que reciben la información necesaria para ser renderizados, y comunican eventos hacia sus componentes padre.
* pages: Representan las vistas a las que tendrá acceso el usuario y se construyen a partir de los componentes del primer grupo.

También se cuenta con las carpetas auxiliares:

* services: Contiene la funcionalidad que lanza y procesa las peticiones que se realizan al backend.
* styles: Contiene definiciones comunes para la construcción de toda la app: Estilos base, variables y mixins.


## SASS Variables

Incluí un archivo SASS que incluye variables para representar las definiciones comunes del diseño: Colores, espaciados, bordes, etc, lo que permite administrarlas desde un único punto, estando disponible para todos los componentes de la app en cuanto lo necesiten.


## Storybook

Acompañé el desarrollo de los componentes individuales con el uso de Storybook para facilitar realizar pruebas rápidas, de manera independiente, y que me permitieran conformar su estructura lo mejor posible.


## Diseño responsivo

Si bien los insumos no incluían la definición visual de cómo se vería la app en dispositivos móviles, plantee la estructura y los componentes de tal manera que pudiera lograr un resultado visual e interactivo agradable para pantallas más pequeñas.

Es por esto que los estilos se construyeron siguiendo algunas de las recomendaciones del *mobile-first*.



# Consideraciones solicitadas

## Documentación

Este archivo describe puntos importantes del proceso que realicé durante la prueba.


## Usabilidad

* Uso de elementos que indican una operación en proceso (Loaders).
* Se informa al usuario cuando ocurre un error.
* Se informa al usuario cuando no se obtuvo un resultado.
* El cuadro de búsqueda reacciona a la tecla Enter.
* Diseño responsivo para permitir su uso en diferentes tipos de dispositivos.

## SEO

* HTML Semántico integrado con los componentes.
* Las rutas son amigables.

## Performance

* Se utiliza compression en el backend para que la respuesta utilice gzip.
* Los componentes que utilizan useEffect incluyen filtros para renderizarse sólo cuando alguna propiedad importante cambia.

## Escalabilidad

* Estructuras modulares que permiten tener funcionalidades separadas e independientes que se integran con la app a medida que se requieren.
* El uso de elementos comunes, tanto en front como en back, permiten construir nuevos módulos sin tener que replicar las tareas más comúnes, y concentrarse en las responsabilidades particulares de cada funcionalidad.

