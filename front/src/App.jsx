import { useState } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import {
  Home,
  SearchResult,
  ProductDetails,
  NotFoundPage
} from './pages/'
import Header from './components/Header'
import './App.scss'


function App() {
  const [query, setQuery] = useState('')

  const onUpdateSearch = value => {
    setQuery(value || '')
  }

  return (
    <div className="app meli-test">
      <Router>
        <Header activeQuery={query} onSearch={onUpdateSearch} />

        <div className="app-container">
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>

            <Route exact path="/items">
              <SearchResult />
            </Route>

            <Route path="/items/:id">
              <ProductDetails />
            </Route>

            <Route>
              <NotFoundPage />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
