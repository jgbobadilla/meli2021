import { Link, useHistory } from 'react-router-dom'
import PropTypes from 'prop-types'
import './Header.scss'

const KEY_ENTER = 13;

const Header = ({ activeQuery, onSearch }) => {
  const history = useHistory()

  const emitSearch = value => {
    onSearch(value)

    if (value) {
      history.push(`/items?search=${value}`)
    } else {
      history.push('/')
      resetInput()
    }
  }

  const clearInput = (value) => {
    const cleared = (value || '').trim()
    document.getElementById('search-input').value = cleared

    return cleared
  }

  const resetInput = () => {
    document.getElementById('search-input').value = ''
  }

  const onButtonClick = event => {
    const value = clearInput(document.getElementById('search-input').value)
    value ? emitSearch(value): resetInput()
  }

  const onInputChange = event => {
    if (event.charCode && event.charCode === KEY_ENTER) {
      const value = clearInput(event.target.value)
      value ? emitSearch(value): resetInput()
    }
  }  

  return (
    <header className="header" role="banner">
      <div className="header__wrapper">
        <Link to="/">
          <img className="logo"
            src={`${process.env.PUBLIC_URL}/images/logo_ml.png`}
            alt="Mercado Libre Home"/>
        </Link>

        <div className="search-box">
          <input type="search"
                className="search-box__input"
                name="search-input"
                id="search-input"
                placeholder="Nunca dejes de buscar"
                defaultValue={activeQuery || ''}
                onKeyPress={e => onInputChange(e)} />

          <button className="search-box__button"
                  onClick={e => onButtonClick(e)}>
            <img src={`${process.env.PUBLIC_URL}/images/ic_search.png`}
              alt=""/>
          </button>
        </div>
      </div>
    </header>
  )
}

Header.propTypes = {
  activeQuery: PropTypes.string,
  onSearch: PropTypes.func
}

export default Header
