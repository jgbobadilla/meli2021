import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './Breadcrumb.scss'

const renderLevel = ({ id, name }) => 
  <li key={id} className="breadcrumb__item">
    <Link to={`/categories/${id}`}
          className="breadcrumb__link">
      <span className="breadcrumb__name">{name}</span>
    </Link>
  </li>

const Breadcrumb = ({ levels }) => {
  return (
    !!levels.length && 
    (<nav className="breadcrumb">
      <ul className="breadcrumb__list">
        {levels && levels.map(level => renderLevel(level))}
      </ul>
    </nav>)
  )
}

Breadcrumb.propTypes = {
  levels: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired
}

Breadcrumb.defaultValues = {
  levels: []
}

export default Breadcrumb
