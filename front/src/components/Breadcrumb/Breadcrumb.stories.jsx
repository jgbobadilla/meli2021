import Breadcrumb from './Breadcrumb'

const MOCK_LEVELS = [
  {
    id: '1',
    name: 'Electrónica, Audio y Video'
  },
  {
    id: '2',
    name: 'iPod'
  },
  {
    id: '3',
    name: 'Reproductores'
  },
  {
    id: '4',
    name: 'iPod touch'
  },
  {
    id: '5',
    name: '32 GB'
  },
]

export default {
  title: 'ML Breadcrumb',
  component: Breadcrumb
}

export const BreadcrumbExample = () => 
  <Breadcrumb levels={MOCK_LEVELS} />
