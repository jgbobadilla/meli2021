import PropTypes from 'prop-types'
import './ProductDetail.scss'


const renderPricing = ({ amount, currency }) => {
  return <div className="product-detail__pricing">
    <span className="product-detail__currency">{currency}</span>
    <span className="product-detail__price">{amount}</span>
  </div>
}

const ProductDetail = ({ product }) => {
  const { title, description, price, picture, condition, sold_quantity } = product

  return (
    <article className="product-detail">
      <header className="product-detail__header">
        <figure className="product-detail__picture">
          <img src={picture} alt={title}/>
        </figure>

        <div className="product-detail__data">
          <div className="product-detail__status">
            <span>{condition === "new" ? "Nuevo" : "Usado"}</span> -
            <span>{sold_quantity || 0} vendidos</span>  
          </div>

          <h2 className="product-detail__title">
            {title}
          </h2>

          {renderPricing(price)}

          <button className="btn btn--primary btn--w100">
            Comprar
          </button>
        </div>
      </header>

      <section className="product-detail__content">
        <h3 className="product-detail__description-title">
          Descripción del producto
        </h3>

        <p className="product-detail__description">
          {description}
        </p>
      </section>
    </article>
  )
}

ProductDetail.propTypes = {
  product: PropTypes.shape({}).isRequired,
}

export default ProductDetail
