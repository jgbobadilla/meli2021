import ProductDetail from './ProductDetail';


const PRODUCT_MOCK = {
  id: "MLA659286619",
  title: "Brawn Gp01 2009 Jenson Button 1/43 Coleccion Formula1 Salvat",
  price: {
    currency: "ARS",
    price: 3499
  },
  condition: "new",
  unitsSold: 234,
  picture: "http://http2.mlstatic.com/D_855063-MLA25637875331_062017-O.jpg",
  pictureId: "855063-MLA25637875331_062017",
  state: 'Buenos Aires',
  freeShipping: false,
  description: "Brawn GP01 2009 Jenson Button 1/43 Coleccion Formula1 Salvat fasciculo n° 6. Está cerrado en su caja sin abrir, incluye su revista correspondiente se retira a dos cuadras de lavalle y florida microcentro o se realizan envios por mercado envios compre tranquilo que tengo 100% de calificaciones positivas"
}

export default {
  title: "ML Product Detail",
  component: ProductDetail
}

export const ProductDetailExample = () =>
  <ProductDetail product={PRODUCT_MOCK} />
