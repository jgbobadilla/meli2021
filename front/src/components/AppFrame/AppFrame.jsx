import PropTypes from 'prop-types'
import Header from './../Header'
import './AppFrame.scss'


const AppFrame = ({ children, isCentered }) => {
  return (
    <div className="app-frame">
      <main className={"app-content" + (isCentered ? ' app-content--centered' : '')} >
        { children }
      </main>
    </div>
  )
}

AppFrame.propTypes = {
  children: PropTypes.node,
  isCentered: PropTypes.bool
}

AppFrame.defaultValues = {
  isCentered: false
}

export default AppFrame
