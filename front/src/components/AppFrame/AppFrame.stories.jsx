import AppFrame from './AppFrame'

export default {
  title: "ML App Frame",
  component: AppFrame
}

export const AppFrameExample = () =>
  <AppFrame>
    <span>Content</span>
  </AppFrame>

export const AppFrameEmptyExample = () =>
  <AppFrame />