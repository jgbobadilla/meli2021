import PropTypes from 'prop-types'
import ResultListItem from './../ResultListItem';
import './ResultList.scss';


const ResultList = ({ results }) => {
  return (
    results && results.length ?
      (<section className="result-list">
        { results.map(result =>
          (<ResultListItem key={result.id}
                          result={result} />)) }
      </section>) : 
      (<div>No se encontraron resultados. Intenta otras palabras.</div>)
  )
}

ResultList.propTypes = {
  results: PropTypes.arrayOf(PropTypes.object).isRequired,
}

ResultList.defaultValues = {
  results: []
}

export default ResultList
