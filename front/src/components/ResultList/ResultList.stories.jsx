import ResultList from './ResultList'
import './ResultList.scss'

const RESULTS_MOCK = [
  {
    id: "MLA659286619",
    title: "Brawn Gp01 2009 Jenson Button 1/43 Coleccion Formula1 Salvat",
    price: {
      currency: "ARS",
      amount: 3499,
      decimals: 0
    },
    condition: "new",
    picture: "http://http2.mlstatic.com/D_855063-MLA25637875331_062017-O.jpg",
    pictureId: "855063-MLA25637875331_062017",
    state: 'Buenos Aires',
    freeShipping: false
  },
  {
    id: "MLA913281552",
    title: "F1 Circus 91 Hucard Para Pc-engine/turbografx",
    price: {
      currency: "ARS",
      amount: 2500,
      decimals: 0
    },
    condition: "used",
    picture: "http://http2.mlstatic.com/D_841939-MLA45311758665_032021-I.jpg",
    pictureId: "841939-MLA45311758665_032021",
    state: 'Buenos Aires',
    freeShipping: false
  },
  {
    id: "MLA913060448",
    title: "Mclaren Mp4/23 2008 Hamilton - Colección Formula1 Salvat",
    price: {
      currency: "ARS",
      amount: 2200,
      decimals: 0
    },
    condition: "used",
    picture: "http://http2.mlstatic.com/D_705706-MLA45296333072_032021-O.jpg",
    pictureId: "705706-MLA45296333072_032021",
    state: 'Capital Federal',
    freeShipping: false
  },
  {
    id: "MLA805760391",
    title: "Tut Tut Bolidos Surtidos Luces Y Sonido Vtech",
    price: {
      currency: "ARS",
      amount: 1699,
      decimals: 9
    },
    condition: "new",
    picture: "http://http2.mlstatic.com/D_706139-MLA43241494530_082020-I.jpg",
    pictureId: "706139-MLA43241494530_082020",
    state: 'Mendoza',
    freeShipping: false
  }
]

export default {
  title: "ML Result",
  component: ResultList
}

export const ResultListExample = () => 
  <ResultList results={RESULTS_MOCK} />
