import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import './ResultListItem.scss'

const formatDecimalPortion = (amount, decimals = 2) => {
  try {
    const amountString = amount.toString()
    const dotIndex = amountString.indexOf('.')

    if (dotIndex < 0) return '00'

    return amount.toFixed(decimals).substr(dotIndex + 1, 2)
  } catch (e) {
    return '00'
  }
}

const renderFreeShippingIcon = isFreeShipping => {
  return isFreeShipping &&
    <img className="result__shipping--free"
        src={`${process.env.PUBLIC_URL}/images/ic_shipping.png`}
        alt="Envío gratis para este producto"/>
}

const renderPricing = isFreeShipping => ({ amount, decimals, currency }) => {
  if (!amount || !currency)
    return ''

  return <div className="result__pricing">
    <span className="result__currency">{currency}</span>
    <span className="result__amount">{parseInt(amount)}.</span>
    <span className="result__decimals">{`${formatDecimalPortion(amount, decimals)}`}</span>
    {renderFreeShippingIcon(isFreeShipping)}
  </div>
}

const ResultListItem = ({ result }) => {
  const {id, title, price, picture, state, free_shipping} = result

  return (
    <Link to={`/items/${id}`}>
      <article className="result">
        <figure className="result__picture">
          <img src={picture} alt={title}/>
        </figure>

        <div className="result__data">
          <div className="result__description">
            {renderPricing(free_shipping)(price)}

            <span className="result__city">{state}</span>
          </div>

          <h2 className="result__name">
            {title}
          </h2>
        </div>
      </article>
    </Link>
  )
}

ResultListItem.propTypes = {
  result: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.shape({
      currency: PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired,
      decimals: PropTypes.number.isRequired,
    }).isRequired,
    picture: PropTypes.string.isRequired,
    condition: PropTypes.string,
    freeShipping: PropTypes.bool,
  }).isRequired
}

export default ResultListItem
