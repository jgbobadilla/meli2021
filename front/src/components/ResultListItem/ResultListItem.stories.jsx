import ResultListItem from './ResultListItem'

const RESULT_MOCK = {
  id: "MLA659286619",
  title: "Brawn Gp01 2009 Jenson Button 1/43 Coleccion Formula1 Salvat",
  price: {
    currency: "ARS",
    amount: 3499,
    decimals: 0
  },
  condition: "new",
  picture: "http://http2.mlstatic.com/D_855063-MLA25637875331_062017-O.jpg",
  pictureId: "855063-MLA25637875331_062017",
  state: 'Buenos Aires',
  freeShipping: false
}

export default {
  title: "ML Result Item",
  component: ResultListItem
}

export const ResultListItemExample = () => 
  <ResultListItem result={RESULT_MOCK} />

export const ResultListItemFreeShippingExample = () => 
  <ResultListItem result={{...RESULT_MOCK, freeShipping: true}} />
