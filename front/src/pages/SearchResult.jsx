import { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import ItemsAPI from './../services/apiItemsService'
import AppFrame from './../components/AppFrame'
import Breadcrumb from './../components/Breadcrumb'
import ResultList from './../components/ResultList'


const SearchResult = () => {
  const [levels, setLevels] = useState([])
  const [results, setResults] = useState([])
  const [error, setError] = useState(void 0)
  const [isLoading, setIsLoading] = useState(true)

  const query = new URLSearchParams(useLocation().search).get('search')

  useEffect(() => {
    const fetchResults = async () => {
      try {
        const { categories, items } = await ItemsAPI.searchItems(query)
  
        setLevels(categories)
        setResults(items)
        setError(void 0)
      } catch (err) {
        setLevels([])
        setResults([])
        setError(err)
      } finally {
        setIsLoading(false)
      }
    }

    fetchResults()
  }, [query])

  return (
    <AppFrame isCentered={!!error || isLoading || !results.length}>
      { isLoading && (
        <div>Cargando...</div>
      )}

      { error && (
        <div>Tenemos un problema para realizar tu búsqueda. Intenta otras palabras.</div>
      )}

      { !error && !isLoading && (
        <Breadcrumb levels={levels}>
        </Breadcrumb>
      )}

      { !error && !isLoading && (
        <ResultList results={results}>
        </ResultList>
      )}
    </AppFrame>
  )
}

export default SearchResult
