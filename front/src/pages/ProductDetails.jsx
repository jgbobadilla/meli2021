import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import ItemsAPI from './../services/apiItemsService'
import AppFrame from './../components/AppFrame'
import ProductDetail from './../components/ProductDetail';


const ProductDetails = () => {
  const [product, setProduct] = useState({})
  const [error, setError] = useState(void 0)
  const [isLoading, setIsLoading] = useState(true)

  const { id } = useParams()

  useEffect(() => {
    const fetchProduct = async () => {
      try {
        const { item } = await ItemsAPI.getItem(id)
        setProduct(item)
        setError(void 0)
      } catch (err) {
        setProduct(void 0)
        setError(err)
      } finally {
        setIsLoading(false)
      }
    }

    fetchProduct()
  }, [])

  return (
    <AppFrame isCentered={!!error || isLoading || !product}>
      { isLoading && (
        <div>Cargando...</div>
      )}

      { !!error && (
        <div>Tenemos un problema para cargar el producto. Intenta en unos minutos.</div>
      )}

      {
        product && product.id ? 
          (<ProductDetail product={product} />) :
          ( !(isLoading || error) && <div>No existe el producto solicitado.</div>)
      }
    </AppFrame>
  )
}

ProductDetails.propTypes = {

}

export default ProductDetails
