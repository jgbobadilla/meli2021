import AppFrame from '../components/AppFrame'
import './Home.scss'


const Home = () => {
  return (
    <AppFrame isCentered={true}>
      <div className="home">

        <p className="home__intro">
          Escribe en el buscador lo que quieres encontrar.
        </p>

        <ul className="home__instructions">
          <li>Escribe tu búsqueda en el campo que figura en la parte superior de la pantalla.</li>
          <li>Navega por categorías de productos para encontrar el producto que buscas.</li>
        </ul>
      </div>
    </AppFrame>
  )
}

export default Home
