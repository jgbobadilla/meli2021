export {default as Home} from './Home'
export {default as SearchResult} from './SearchResult'
export {default as ProductDetails} from './ProductDetails'
export {default as NotFoundPage} from './NotFoundPage'
