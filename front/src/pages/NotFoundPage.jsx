import AppFrame from './../components/AppFrame'
import './NotFoundPage.scss'

export const NotFoundPage = () => {
  return (
    <AppFrame isCentered="true">
      <div className="not-found">
        404 - Página no disponible
      </div>
    </AppFrame>
  )
}

export default NotFoundPage