import axios from 'axios';

const itemService = axios.create({
  baseURL: 'http://localhost:3100/api/items'
})

export const searchItems = async (query) => {
  const { data } = await itemService.get('/', {
    params: {
      q: query
    }
  })

  return data
}

export const getItem = async (itemId) => {
  const { data } = await itemService.get(`/${itemId}`)

  return data
}

const ItemsAPI = {
  getItem,
  searchItems
}

export default ItemsAPI
